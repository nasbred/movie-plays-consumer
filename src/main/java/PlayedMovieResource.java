import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.MoviePlayed;
import org.eclipse.microprofile.reactive.messaging.Channel;

import io.smallrye.mutiny.Multi;

@Path("/movies")
public class PlayedMovieResource {

    @Channel("played-movies")
    Multi<MoviePlayed> playedMovies;

    @GET
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public Multi<MoviePlayed> stream() {
        return playedMovies;
    }

}